<?php

/**
 * @file
 * Enable Drush to wait for new Acquia Cloud tasks to finish before exiting.
 */

/**
 * Current rate limits for Acquia subscriptions by type:
 * - Free: 10/hour
 * - Elite: 1000/hour
 * - Everything else: 360/hour
 */

/**
 * @todo Do we need to increase the PHP time limit?
 * @todo Do we need to put a time cap on how long the checking runs before failing?
 */

define('DRUSH_ACQUIA_WAIT_DELAY_MINIMUM', 5);
define('DRUSH_ACQUIA_WAIT_DELAY_DEFAULT', 20);

/**
 * Implements hook_drush_command().
 */
function acquia_wait_drush_command() {
  $items = array();

  // @todo Add support for these independent commands at some point.
  $options = acapi_common_options();
  $items['ac-task-wait'] = array(
    'description' => 'Wait for all task within a certain devcloud instance to finish.',
    'callback' => 'drush_acquia_task_wait',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap.
    'arguments' => array(
      'id' => 'A specific task ID or comma-separated list of task IDs to wait for.',
    ),
    'options' => array(
      'wait-delay' => dt("How long to wait in-between checks. Default is @default seconds.", array('@default' => DRUSH_ACQUIA_WAIT_DELAY_DEFAULT)),
      'task-results' => dt("Output the results of the task."),
    ) + $options,
    'examples' => array(
      'drush @mysite-dev ac-task-wait 123456 --wait-delay=30' => 'Wait for task 123456 to complete. Check the status of the task every 30 seconds.',
    ),
  );
  // $items['ac-queue-wait'] = array(
  //   'description' => 'Wait for all task within a certain devcloud instance to finish.',
  //   'callback' => 'drush_acquia_custom_queue_wait',
  //   'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap.
  //   'arguments' => array(
  //     'queue' => 'The queue to wait for.',
  //   ),
  //   'options' => array(
  //     'interval' => 'The time in seconds to delay between each check. Default is 60.',
  //     'ignore-older-than' => 'The time in seconds that a job may be running to ignore. Default is 86400 (one day).',
  //   ) /*+ $common_options*/,
  //   'examples' => array(
  //     'drush @mysite-dev task-wait' => 'Wait for devcloud tasks in queue to finish',
  //   ),
  // );

  return $items;
}

/**
 * Implements hook_drush_help_alter().
 */
function acquia_wait_drush_help_alter(&$command) {
  if (in_array($command['command'], array(
    'ac-database-instance-backup',
    'ac-database-instance-backup-restore',
    'ac-code-deploy',
    'ac-code-path-deploy',
    'ac-database-copy',
    'ac-files-copy',
  ))) {
    $command['options']['wait'] = dt("Wait for the task to finish before existing.");
    $command['options']['wait-delay'] = dt("How long to wait in-between checks. Default is @default seconds.", array('@default' => DRUSH_ACQUIA_WAIT_DELAY_DEFAULT));
    $command['options']['task-results'] = dt("Output the results of the task.");
  }
}

/**
 * Implements drush_hook_post_COMMAND().
 */
function drush_acquia_wait_post_ac_database_instance_backup() {
  acquia_wait_post_invoke();
}

/**
 * Implements drush_hook_post_COMMAND().
 */
function drush_acquia_wait_post_ac_database_instance_backup_restore() {
  acquia_wait_post_invoke();
}

/**
 * Implements drush_hook_post_COMMAND().
 */
function drush_acquia_wait_post_ac_code_deploy() {
  acquia_wait_post_invoke();
}

/**
 * Implements drush_hook_post_COMMAND().
 */
function drush_acquia_wait_post_ac_code_path_deploy() {
  acquia_wait_post_invoke();
}

/**
 * Implements drush_hook_post_COMMAND().
 */
function drush_acquia_wait_post_ac_database_copy() {
  acquia_wait_post_invoke();
}

/**
 * Implements drush_hook_post_COMMAND().
 */
function drush_acquia_wait_post_ac_files_copy() {
  acquia_wait_post_invoke();
}

function acquia_wait_post_invoke() {
  if (drush_get_option('wait')) {
    if ($task_ids = acquia_wait_get_new_task_ids()) {
      drush_unset_option('wait');
      drush_invoke('ac-task-wait', array($task_ids));
    }
    else {
      drush_set_error('AC_TASK_WAIT_NO_NEW_TASKS', dt('Could not find new expected task IDs in the Drush log.'));
    }
  }
}

/**
 * Collect new tasks from the logs and wait for them.
 */
function drush_acquia_task_wait($task_ids) {
  $task_ids = is_array($task_ids) ? $task_ids : explode(',', $task_ids);
  $tasks = array_combine($task_ids, $task_ids);
  foreach ($task_ids as $task_id) {
    $tasks[$task_id] .= ' (started)';
  }

  $delay = intval(drush_get_option('wait-delay', DRUSH_ACQUIA_WAIT_DELAY_DEFAULT));
  if ($delay < DRUSH_ACQUIA_WAIT_DELAY_MINIMUM) {
    drush_log(dt('The wait-delay option cannot be less than @minimum seconds so it has been reset.', array('@minimum' => DRUSH_ACQUIA_WAIT_DELAY_MINIMUM)), 'warning');
    $delay = DRUSH_ACQUIA_WAIT_DELAY_MINIMUM;
  }
  $first_delay = $delay;

  // Show how long on average these tasks are taking in the last month.
  /*if ($historical = acquia_wait_get_expected_time($task_ids)) {
    foreach ($historical as $task_description => $task_average) {
      drush_log(dt("Expected time for completion of task @task is @average.", array('@task' => $task_description, '@average' => _acquia_wait_format_interval(round($task_average)))), 'ok');
    }
    // If there is historical data, use it to set a reasonable first delay.
    $first_delay = max(ceil(max($historical) / 10) * 10, DRUSH_ACQUIA_WAIT_DELAY_MINIMUM);
  }*/

  drush_log(dt("Initial delay of @delay second(s).", array('@delay' => $first_delay)), 'info');
  sleep($first_delay);

  do {
    foreach ($tasks as $task_id => $summary) {
      $info = acquia_wait_get_task_info($task_id);
      if (empty($info)) {
        drush_set_error('AC_TASK_WAIT_NO_INFO', "Unable to fetch data about task $task_id.");
        unset($tasks[$task_id]);
        continue;
      }

      drush_log(dt("Fetched data about task @task. The current state is: @state.", array('@task' => $task_id, '@state' => $info->state)), 'info');

      switch ($info->state) {
        case 'done':
          if (!empty($info->logs) && (drush_get_option('task-results') || drush_get_context('DRUSH_VERBOSE'))) {
            drush_print($info->logs);
          }
          $info->elapsed = intval($info->completed) - intval($info->started);
          drush_log(dt("Task @task completed in @elapsed.", array('@task' => $task_id, '@elapsed' => _acquia_wait_format_interval($info->elapsed))), 'success');
          unset($tasks[$task_id]);
          continue 2;

        case 'error':
        case 'failed':
          if (!empty($info->logs) && (drush_get_option('task-results') || drush_get_context('DRUSH_VERBOSE'))) {
            drush_print($info->logs);
          }
          drush_set_error('AC_TASK_WAIT_ERROR', dt('Task @task returned @status status.', array('@task' => $task_id, '@status' => $info->state)));
          unset($tasks[$task_id]);
          continue 2;

        default:
          $tasks[$task_id] = $task_id . ' (' . $info->state . ')';
      }
    }

    if (!empty($tasks)) {
      drush_log(dt('Waiting on tasks @tasks. Checking again in @delay seconds.', array('@tasks' => implode(', ', $tasks), '@delay' => $delay)), 'info');
      sleep($delay);
    }
  }
  while (!empty($tasks));
}

function acquia_wait_get_new_task_ids() {
  $task_ids = array();

  // Because acapi.drush.inc logs the new tasks, we can inspect it!
  $logs = drush_get_context('DRUSH_LOG', array());
  foreach ($logs as $log) {
    if (preg_match('/Task (\d+) started/', $log['message'], $matches)) {
      $task_ids[] = $matches[1];
    }
  }

  return array_unique($task_ids);
}

function acquia_wait_get_task_list() {
  // Ideally I'd like to be using the following, but has not been working in testing:
  // $options = drush_redispatch_get_options();
  // unset($options['wait']);
  // $options['format'] = 'json';
  // $results = drush_invoke_process('@self', 'ac-task-list', array(), $options, array('integrate' => FALSE));

  // Because this runs after the actual Acquia command, using acapi_call() is safe to rely on.
  if ($results = acapi_call('GET', '/sites/:realm::site/tasks', acapi_get_site_args(), array(), array(), array('display' => FALSE))) {
    return $results[1];
  }
}

function acquia_wait_get_task_info($task_id) {
  // Ideally I'd like to be using the following, but has not been working in testing:
  // $options = drush_redispatch_get_options();
  // unset($options['wait']);
  // unset($options['wait-delay']);
  // $options['format'] = 'json';
  // $results = drush_invoke_process('@self', 'ac-task-info', array($task_id), $options, array('integrate' => FALSE));

  // Because this runs after the actual Acquia command, using acapi_call() is safe to rely on.
  $api_args = acapi_get_site_args();
  $api_args[":task"] = $task_id;
  if ($results = acapi_call('GET', '/sites/:realm::site/tasks/:task', $api_args, array(), array(), array('display' => FALSE))) {
    if (drush_get_context('DRUSH_DEBUG')) {
      drush_print(print_r($results, TRUE));
    }
    return $results[1];
  }
}

function acquia_wait_get_expected_time(array $task_ids) {
  $data = array();
  $data_keys = array();
  if ($tasks = acquia_wait_get_task_list()) {
    foreach ($tasks as $task) {
      // Collect data on any tasks that were completed in the last month.
      if ($task->state === 'done' && intval($task->completed) > (time() - 2592000)) {
        $data += array($task->description => array());
        $data[acquia_wait_normalize_description($task)][] = (intval($task->completed) - intval($task->started));
      }
      if (in_array($task->id, $task_ids)) {
        $data_keys[acquia_wait_normalize_description($task)] = $task->id;
      }
    }
  }

  $return = array();
  $data = array_intersect_key($data, $data_keys);
  foreach ($data_keys as $key => $task_id) {
    $return[$task_id . ' (' . $key . ')'] = (array_sum($data[$key]) / count($data[$key]));
  }
  return $return;
}

function acquia_wait_normalize_description($task) {
  return rtrim(strtolower($task->description), '.');
}

function acquia_wait_set_time_limit($time_limit) {
  if (function_exists('set_time_limit')) {
    $current = ini_get('max_execution_time');
    // Do not set time limit if it is currently unlimited.
    if ($current != 0) {
      drush_log(dt("Calling set_time_limit(@limit)", array('@limit' => $time_limit)), 'info');
      @set_time_limit($time_limit);
    }
  }
}

function _acquia_wait_format_interval($interval, $granularity = 2) {
  $units = array(
    '1 year|@count years' => 31536000,
    '1 month|@count months' => 2592000,
    '1 week|@count weeks' => 604800,
    '1 day|@count days' => 86400,
    '1 hour|@count hours' => 3600,
    '1 min|@count min' => 60,
    '1 sec|@count sec' => 1
  );
  $output = '';
  foreach ($units as $key => $value) {
    $key = explode('|', $key);
    if ($interval >= $value) {
      // format_plural() not always available.
      $output .= ($output ? ' ' : '') . dt($key[1], array('@count' => floor($interval / $value)));
      $interval %= $value;
      $granularity--;
    }

    if ($granularity == 0) {
      break;
    }
  }
  return $output ? $output : dt('0 sec', array());
}
